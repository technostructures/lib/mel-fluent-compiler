// Generated by Melange
'use strict';

var Belt_Array = require("melange.belt/./belt_Array.js");

function build_ast(node) {
  var match = node.type;
  switch (match) {
    case "Annotation" :
        return {
                TAG: /* Annotation */23,
                _0: {
                  code: node.code,
                  arguments: node.arguments,
                  message: node.message
                }
              };
    case "Attribute" :
        return {
                TAG: /* Attribute */14,
                _0: {
                  id: build_ast(node.id),
                  value: bind_null(node.value),
                  attributes: [],
                  comment: null
                }
              };
    case "CallArguments" :
        return {
                TAG: /* CallArguments */13,
                _0: {
                  positional: Belt_Array.map(node.positional, build_ast),
                  named: Belt_Array.map(node.named, build_ast)
                }
              };
    case "Comment" :
        return {
                TAG: /* Comment */18,
                _0: {
                  content: node.content
                }
              };
    case "FunctionReference" :
        return {
                TAG: /* FunctionReference */11,
                _0: {
                  id: build_ast(node.id),
                  arguments: build_ast(node.arguments)
                }
              };
    case "GroupComment" :
        return {
                TAG: /* GroupComment */19,
                _0: {
                  content: node.content
                }
              };
    case "Identifier" :
        return {
                TAG: /* Identifier */17,
                _0: {
                  name: node.name
                }
              };
    case "Junk" :
        return {
                TAG: /* Junk */21,
                _0: {
                  annotations: Belt_Array.map(node.annotations, build_ast),
                  content: node.content
                }
              };
    case "Message" :
        return {
                TAG: /* Message */1,
                _0: {
                  id: build_ast(node.id),
                  value: bind_null(node.value),
                  attributes: Belt_Array.map(node.attributes, build_ast),
                  comment: bind_null(node.comment)
                }
              };
    case "MessageReference" :
        return {
                TAG: /* MessageReference */8,
                _0: {
                  id: build_ast(node.id),
                  attribute: bind_null(node.attribute)
                }
              };
    case "NamedArgument" :
        return {
                TAG: /* NamedArgument */16,
                _0: {
                  name: build_ast(node.name),
                  value: build_ast(node.value)
                }
              };
    case "NumberLiteral" :
        return {
                TAG: /* NumberLiteral */7,
                _0: {
                  value: node.value
                }
              };
    case "Pattern" :
        return {
                TAG: /* Pattern */3,
                _0: {
                  elements: Belt_Array.map(node.elements, build_ast)
                }
              };
    case "Placeable" :
        return {
                TAG: /* Placeable */5,
                _0: {
                  expression: build_ast(node.expression)
                }
              };
    case "Resource" :
        return {
                TAG: /* Resource */0,
                _0: {
                  body: Belt_Array.map(node.body, build_ast)
                }
              };
    case "ResourceComment" :
        return {
                TAG: /* ResourceComment */20,
                _0: {
                  content: node.content
                }
              };
    case "SelectExpression" :
        return {
                TAG: /* SelectExpression */12,
                _0: {
                  selector: build_ast(node.selector),
                  variants: Belt_Array.map(node.variants, build_ast)
                }
              };
    case "Span" :
        return {
                TAG: /* Span */22,
                _0: {
                  start: node.start,
                  _end: node._end
                }
              };
    case "StringLiteral" :
        return {
                TAG: /* StringLiteral */6,
                _0: {
                  value: node.value
                }
              };
    case "Term" :
        return {
                TAG: /* Term */2,
                _0: {
                  id: build_ast(node.id),
                  value: bind_null(node.value),
                  attributes: Belt_Array.map(node.attributes, build_ast),
                  comment: bind_null(node.comment)
                }
              };
    case "TermReference" :
        return {
                TAG: /* TermReference */9,
                _0: {
                  id: build_ast(node.id),
                  attribute: bind_null(node.attribute),
                  arguments: bind_null(node.arguments)
                }
              };
    case "TextElement" :
        return {
                TAG: /* TextElement */4,
                _0: {
                  value: node.value
                }
              };
    case "VariableReference" :
        return {
                TAG: /* VariableReference */10,
                _0: {
                  id: build_ast(node.id)
                }
              };
    case "Variant" :
        return {
                TAG: /* Variant */15,
                _0: {
                  key: build_ast(node.key),
                  value: build_ast(node.value),
                  default: node.default
                }
              };
    default:
      return {
              TAG: /* Junk */21,
              _0: {
                annotations: [],
                content: ""
              }
            };
  }
}

function bind_null(nullable) {
  if (nullable == null) {
    return nullable;
  } else {
    return build_ast(nullable);
  }
}

function bind_array(arr) {
  return Belt_Array.map(arr, build_ast);
}

exports.build_ast = build_ast;
exports.bind_null = bind_null;
exports.bind_array = bind_array;
/* No side effect */
