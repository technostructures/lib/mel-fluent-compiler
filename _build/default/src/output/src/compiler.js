// Generated by Melange
'use strict';

var Belt_List = require("melange.belt/./belt_List.js");
var Belt_Array = require("melange.belt/./belt_Array.js");
var Belt_Option = require("melange.belt/./belt_Option.js");
var Belt_MapString = require("melange.belt/./belt_MapString.js");
var Melange__Parser = require("./parser.js");
var Melange__Simplifier = require("./simplifier.js");

function string_of_literal(str) {
  switch (str.TAG | 0) {
    case /* LiteralString */0 :
        return "\"" + (str._0 + "\"");
    case /* LiteralInt */1 :
    case /* LiteralFloat */2 :
        return str._0;
    
  }
}

function builtin_name(name) {
  switch (name) {
    case "DATETIME" :
        return "Fluent.datetime_format";
    case "NUMBER" :
        return "Fluent.number_format";
    default:
      return name;
  }
}

function build_param(param) {
  return param[0] + (" = " + param[1]);
}

function build_builtin_param(param) {
  return "~" + (param[0] + (":" + param[1]));
}

function build_params(params) {
  var match = params.length;
  if (match === 0) {
    return "";
  }
  var formatted_params = Belt_Array.map(params, build_param).join("; ");
  return " { " + (formatted_params + " }");
}

function build_builtin_params(name, params) {
  var params_maker;
  switch (name) {
    case "DATETIME" :
        params_maker = "Fluent.Runtime.make_datetime_params";
        break;
    case "NUMBER" :
        params_maker = "Fluent.Runtime.make_number_params";
        break;
    default:
      params_maker = name;
  }
  var match = params.length;
  var formatted_params = match !== 0 ? Belt_Array.map(params, build_builtin_param).join(" ") : "";
  return "(" + (params_maker + (" " + (formatted_params + "())")));
}

function extract_type_param(other) {
  if (other.TAG !== /* BuiltInRef */4) {
    return [
            "",
            other
          ];
  }
  var params = other._2;
  var subject = other._1;
  var name = other._0;
  if (name !== "NUMBER") {
    return [
            "",
            {
              TAG: /* BuiltInRef */4,
              _0: name,
              _1: subject,
              _2: params
            }
          ];
  }
  var match = Belt_Array.reduce(params, [
        "",
        []
      ], (function (param, param$1) {
          var value = param$1[1];
          var name = param$1[0];
          var new_params = param[1];
          if (name === "type") {
            return [
                    " ~_type:" + value,
                    new_params
                  ];
          }
          new_params.push([
                name,
                value
              ]);
          return [
                  "",
                  new_params
                ];
        }));
  return [
          match[0],
          {
            TAG: /* BuiltInRef */4,
            _0: name,
            _1: subject,
            _2: match[1]
          }
        ];
}

function build_locale_getter(locale_getter) {
  if (locale_getter == null) {
    return "lc";
  } else {
    return locale_getter;
  }
}

function build_locale_argument(locale_getter) {
  if (locale_getter == null) {
    return "lc";
  } else {
    return "";
  }
}

function remove_builtin(other) {
  if (other.TAG === /* BuiltInRef */4) {
    return other._1;
  } else {
    return other;
  }
}

function build_expression(locale_getter, l) {
  switch (l.TAG | 0) {
    case /* Literal */0 :
        return string_of_literal(l._0);
    case /* Select */1 :
        return build_select(locale_getter, l._0, l._1);
    case /* VariableRef */2 :
        return "params." + l._0;
    case /* FunctionRef */3 :
        return "(" + (l._0 + (build_params(l._1) + (" " + (build_locale_argument(locale_getter) + ")"))));
    case /* BuiltInRef */4 :
        var name = l._0;
        return "(" + (builtin_name(name) + (" " + (build_expression(locale_getter, l._1) + (" " + (build_builtin_params(name, l._2) + (" " + (build_locale_getter(locale_getter) + ")")))))));
    
  }
}

function build_pattern_element(locale_getter, text) {
  if (text.TAG === /* TextElement */0) {
    return "{js|" + (text._0 + "|js}");
  } else {
    return build_expression(locale_getter, text._0);
  }
}

function build_select(locale_getter, selector, pattern_array_with_default) {
  if (selector.TAG === /* BuiltInRef */4 && selector._0 === "NUMBER") {
    return build_select_number(locale_getter, selector, pattern_array_with_default);
  } else {
    return build_select_other(locale_getter, selector, pattern_array_with_default);
  }
}

function build_select_number(locale_getter, selector, pattern_array_with_default) {
  var match = extract_type_param(selector);
  var selector$1 = match[1];
  var selector_without_builtin = remove_builtin(selector$1);
  var built_selector = build_expression(locale_getter, selector$1);
  var selector$2 = "(" + (built_selector + (", Fluent.plural_rule " + (build_expression(locale_getter, selector_without_builtin) + (" lc" + (match[0] + ")")))));
  var patterns = Belt_Array.map(Belt_Array.keep(pattern_array_with_default, (function (param) {
              return !param[2];
            })), (function (param) {
          var name = param[0];
          return [
                  "\"" + (name + ("\", _ | _, \"" + (name + "\""))),
                  param[1]
                ];
        }));
  var default_pattern = Belt_Option.map(Belt_Array.get(Belt_Array.keep(pattern_array_with_default, (function (param) {
                  return param[2];
                })), 0), (function (param) {
          return [
                  "_, _",
                  param[1]
                ];
        }));
  return build_switch(locale_getter, selector$2, patterns, default_pattern);
}

function build_select_other(locale_getter, selector, pattern_array_with_default) {
  var selector$1 = build_expression(locale_getter, selector);
  var patterns = Belt_Array.map(Belt_Array.keep(pattern_array_with_default, (function (param) {
              return !param[2];
            })), (function (param) {
          return [
                  "\"" + (param[0] + "\""),
                  param[1]
                ];
        }));
  var default_pattern = Belt_Option.map(Belt_Array.get(Belt_Array.keep(pattern_array_with_default, (function (param) {
                  return param[2];
                })), 0), (function (param) {
          return [
                  "_",
                  param[1]
                ];
        }));
  return build_switch(locale_getter, selector$1, patterns, default_pattern);
}

function build_pattern(locale_getter, pattern) {
  return Belt_Array.map(pattern, (function (param) {
                  return build_pattern_element(locale_getter, param);
                })).join(" ^ ");
}

function build_switch_case(locale_getter, param) {
  return "| " + (param[0] + (" ->\n" + build_pattern(locale_getter, param[1])));
}

function build_switch(locale_getter, selector, pattern_array, default_pattern) {
  return "(match " + (selector + (" with\n" + (Belt_Array.map(pattern_array, (function (param) {
                        return build_switch_case(locale_getter, param);
                      })).join("\n") + ((
                  default_pattern !== undefined ? "\n" + build_switch_case(locale_getter, default_pattern) : ""
                ) + ")\n"))));
}

function type_params(param) {
  var inside_record = Belt_Array.map(Belt_MapString.toArray(param.params), (function (param) {
            return param[0] + (" : " + param[1]);
          })).join(" ; ");
  return "type " + (param.name + ("_params = { " + (inside_record + " }\n")));
}

function function_has_params(fn) {
  return !Belt_MapString.isEmpty(fn.params);
}

function build_function_head(fn, locale_getter) {
  var params = Belt_MapString.isEmpty(fn.params) ? "" : " (params : " + (fn.name + "_params)");
  var locale_argument = build_locale_argument(locale_getter);
  var added_unit = params === "" && locale_argument === "" ? "()" : "";
  return "let " + (fn.name + (params + (" " + (locale_argument + (added_unit + " =\n")))));
}

function build_function(default_lc, locale_getter, fn) {
  var type_params$1 = Belt_MapString.isEmpty(fn.params) ? "" : type_params(fn);
  var head = build_function_head(fn, locale_getter);
  var match = fn.bodies.length;
  var body;
  if (match !== 0) {
    if (match !== 1) {
      var patterns = Belt_Array.keep(fn.bodies, (function (param) {
              return param[0] !== default_lc;
            }));
      var default_pattern = Belt_Option.map(Belt_Array.get(Belt_Array.keep(fn.bodies, (function (param) {
                      return param[0] === default_lc;
                    })), 0), (function (param) {
              return [
                      "_",
                      param[1]
                    ];
            }));
      body = build_switch(locale_getter, build_locale_getter(locale_getter), patterns, default_pattern);
    } else {
      body = build_pattern(locale_getter, Belt_Array.getExn(fn.bodies, 0)[1]);
    }
  } else {
    body = "";
  }
  return type_params$1 + (head + body);
}

function build(fn_array, default_lc, locale_getter) {
  return Belt_Array.map(Belt_Array.reverse(Belt_List.toArray(fn_array)), (function (param) {
                  return build_function("\"" + (default_lc + "\""), locale_getter, param[1]);
                })).join("\n\n");
}

function precompile(resource, lc) {
  var ast = Melange__Parser.build_ast(resource);
  return Melange__Simplifier.simplify_ast(lc, ast);
}

function reduce_fn_arrays(acc, fn_array) {
  return Belt_Array.reduce(fn_array, acc, (function (acc2, fn) {
                var f = Belt_List.getAssoc(acc2, fn.name, (function (prim0, prim1) {
                        return prim0 === prim1;
                      }));
                if (f === undefined) {
                  return Belt_List.setAssoc(acc2, fn.name, fn, (function (prim0, prim1) {
                                return prim0 === prim1;
                              }));
                }
                var merged_f_name = f.name;
                var merged_f_bodies = Belt_Array.concat(f.bodies, fn.bodies);
                var merged_f_params = f.params;
                var merged_f_public = f.public;
                var merged_f = {
                  name: merged_f_name,
                  bodies: merged_f_bodies,
                  params: merged_f_params,
                  public: merged_f_public
                };
                return Belt_List.setAssoc(acc2, fn.name, merged_f, (function (prim0, prim1) {
                              return prim0 === prim1;
                            }));
              }));
}

function compile(fn_array_array, default_lc, locale_getter) {
  var merged_array = Belt_Array.reduce(fn_array_array, /* [] */0, reduce_fn_arrays);
  return build(merged_array, default_lc, locale_getter);
}

exports.string_of_literal = string_of_literal;
exports.builtin_name = builtin_name;
exports.build_param = build_param;
exports.build_builtin_param = build_builtin_param;
exports.build_params = build_params;
exports.build_builtin_params = build_builtin_params;
exports.extract_type_param = extract_type_param;
exports.build_locale_getter = build_locale_getter;
exports.build_locale_argument = build_locale_argument;
exports.remove_builtin = remove_builtin;
exports.build_expression = build_expression;
exports.build_pattern_element = build_pattern_element;
exports.build_select = build_select;
exports.build_select_number = build_select_number;
exports.build_select_other = build_select_other;
exports.build_pattern = build_pattern;
exports.build_switch_case = build_switch_case;
exports.build_switch = build_switch;
exports.type_params = type_params;
exports.function_has_params = function_has_params;
exports.build_function_head = build_function_head;
exports.build_function = build_function;
exports.build = build;
exports.precompile = precompile;
exports.reduce_fn_arrays = reduce_fn_arrays;
exports.compile = compile;
/* No side effect */
