type args

type accepted_args = { i : string ; o : string ; d : string Js.Undefined.t ; l : bool }

external process : < argv : args > Js.t = "process" [@@bs.val]

external hide_bin : args -> args = "hideBin" [@@bs.module "yargs/helpers"]

external yargs : args -> < argv : accepted_args > Js.t = "yargs" [@@bs.module]

external glob_sync : string -> string array = "sync" [@@bs.module "glob"]

let () =
  let yargs = process##argv
  |> hide_bin
  |> yargs in
  let args = yargs##argv in
  let file_list = glob_sync args.i in
  let dest = args.o in
  let default_lc = match Js.Undefined.toOption args.d with
  | Some locale -> locale
  | None -> "en"
  in
  let locale_getter = if args.l then Js.Nullable.return "!Locale.get" else Js.Nullable.null in
  Main.process_files file_list dest default_lc locale_getter
