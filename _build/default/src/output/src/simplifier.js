// Generated by Melange
'use strict';

var Belt_List = require("melange.belt/./belt_List.js");
var Belt_Array = require("melange.belt/./belt_Array.js");
var Belt_MapString = require("melange.belt/./belt_MapString.js");

function make_fn_name(namespace, name, $$public) {
  var name$1 = namespace === "" || name === "" ? name : "_" + name;
  var namespace$1 = $$public ? namespace : "_" + namespace;
  var formatted_namespace = namespace$1.replace(/-/g, "_");
  var formatted_name = name$1.replace(/-/g, "_");
  return formatted_namespace + formatted_name;
}

function simplify_identifier(param) {
  switch (param.TAG | 0) {
    case /* NumberLiteral */7 :
        return param._0.value;
    case /* Identifier */17 :
        return param._0.name;
    default:
      throw {
            RE_EXN_ID: "Match_failure",
            _1: [
              "simplifier.ml",
              46,
              26
            ],
            Error: new Error()
          };
  }
}

function simplify_literal(param) {
  switch (param.TAG | 0) {
    case /* StringLiteral */6 :
        return "\"" + (param._0.value + "\"");
    case /* NumberLiteral */7 :
        return param._0.value;
    default:
      throw {
            RE_EXN_ID: "Match_failure",
            _1: [
              "simplifier.ml",
              53,
              23
            ],
            Error: new Error()
          };
  }
}

function get_named_argument(param) {
  if (param.TAG === /* CallArguments */13) {
    return param._0.named;
  }
  throw {
        RE_EXN_ID: "Match_failure",
        _1: [
          "simplifier.ml",
          60,
          25
        ],
        Error: new Error()
      };
}

function simplify_named_arguments(args) {
  return Belt_Array.map(args, (function (param) {
                if (param.TAG === /* NamedArgument */16) {
                  var match = param._0;
                  return [
                          simplify_identifier(match.name),
                          simplify_literal(match.value)
                        ];
                }
                throw {
                      RE_EXN_ID: "Match_failure",
                      _1: [
                        "simplifier.ml",
                        63,
                        22
                      ],
                      Error: new Error()
                    };
              }));
}

function simplify_expression(node, params, in_builtinOpt) {
  var in_builtin = in_builtinOpt !== undefined ? in_builtinOpt : false;
  switch (node.TAG | 0) {
    case /* StringLiteral */6 :
        return {
                TAG: /* Literal */0,
                _0: {
                  TAG: /* LiteralString */0,
                  _0: node._0.value
                }
              };
    case /* NumberLiteral */7 :
        return {
                TAG: /* Literal */0,
                _0: {
                  TAG: /* LiteralInt */1,
                  _0: node._0.value
                }
              };
    case /* MessageReference */8 :
        var match = node._0;
        var attribute = match.attribute;
        var namespace = simplify_identifier(match.id);
        var name = (attribute == null) ? "" : simplify_identifier(attribute);
        var formatted_name = make_fn_name(namespace, name, true);
        return {
                TAG: /* FunctionRef */3,
                _0: formatted_name,
                _1: []
              };
    case /* TermReference */9 :
        var match$1 = node._0;
        var $$arguments = match$1.arguments;
        var attribute$1 = match$1.attribute;
        var namespace$1 = simplify_identifier(match$1.id);
        var name$1 = (attribute$1 == null) ? "" : simplify_identifier(attribute$1);
        var formatted_name$1 = make_fn_name(namespace$1, name$1, false);
        var params$1 = ($$arguments == null) ? [] : simplify_named_arguments(get_named_argument($$arguments));
        return {
                TAG: /* FunctionRef */3,
                _0: formatted_name$1,
                _1: params$1
              };
    case /* VariableReference */10 :
        var name$2 = simplify_identifier(node._0.id);
        var match$2 = Belt_MapString.get(params, name$2);
        if (in_builtin) {
          return {
                  TAG: /* VariableRef */2,
                  _0: name$2
                };
        }
        if (match$2 === undefined) {
          return {
                  TAG: /* VariableRef */2,
                  _0: name$2
                };
        }
        switch (match$2) {
          case "Js.Date.t" :
              return {
                      TAG: /* BuiltInRef */4,
                      _0: "DATETIME",
                      _1: {
                        TAG: /* VariableRef */2,
                        _0: name$2
                      },
                      _2: []
                    };
          case "int" :
              return {
                      TAG: /* BuiltInRef */4,
                      _0: "NUMBER",
                      _1: {
                        TAG: /* VariableRef */2,
                        _0: name$2
                      },
                      _2: []
                    };
          default:
            return {
                    TAG: /* VariableRef */2,
                    _0: name$2
                  };
        }
    case /* FunctionReference */11 :
        var match$3 = node._0;
        var $$arguments$1 = match$3.arguments;
        var name$3 = simplify_identifier(match$3.id);
        var match$4;
        if ($$arguments$1.TAG === /* CallArguments */13) {
          var match$5 = $$arguments$1._0;
          match$4 = [
            Belt_Array.getExn(match$5.positional, 0),
            simplify_named_arguments(match$5.named)
          ];
        } else {
          throw {
                RE_EXN_ID: "Match_failure",
                _1: [
                  "simplifier.ml",
                  114,
                  8
                ],
                Error: new Error()
              };
        }
        return {
                TAG: /* BuiltInRef */4,
                _0: name$3,
                _1: simplify_expression(match$4[0], params, true),
                _2: match$4[1]
              };
    case /* SelectExpression */12 :
        var match$6 = node._0;
        var pattern_array_with_default = Belt_Array.map(match$6.variants, (function (param) {
                if (param.TAG === /* Variant */15) {
                  var match = param._0;
                  var value = match.value;
                  var elements;
                  if (value.TAG === /* Pattern */3) {
                    elements = value._0.elements;
                  } else {
                    throw {
                          RE_EXN_ID: "Match_failure",
                          _1: [
                            "simplifier.ml",
                            126,
                            14
                          ],
                          Error: new Error()
                        };
                  }
                  return [
                          simplify_identifier(match.key),
                          simplify_pattern(elements, params),
                          match.default
                        ];
                }
                throw {
                      RE_EXN_ID: "Match_failure",
                      _1: [
                        "simplifier.ml",
                        123,
                        32
                      ],
                      Error: new Error()
                    };
              }));
        return {
                TAG: /* Select */1,
                _0: simplify_expression(match$6.selector, params, in_builtin),
                _1: pattern_array_with_default
              };
    default:
      throw {
            RE_EXN_ID: "Match_failure",
            _1: [
              "simplifier.ml",
              68,
              2
            ],
            Error: new Error()
          };
  }
}

function simplify_pattern(element_array, params) {
  return Belt_Array.map(element_array, (function (param) {
                switch (param.TAG | 0) {
                  case /* TextElement */4 :
                      return {
                              TAG: /* TextElement */0,
                              _0: param._0.value
                            };
                  case /* Placeable */5 :
                      return {
                              TAG: /* Expression */1,
                              _0: simplify_expression(param._0.expression, params, false)
                            };
                  default:
                    throw {
                          RE_EXN_ID: "Match_failure",
                          _1: [
                            "simplifier.ml",
                            141,
                            20
                          ],
                          Error: new Error()
                        };
                }
              }));
}

function merge_params(_key, maybe_a, maybe_b) {
  var exit = 0;
  var exit$1 = 0;
  if (maybe_a !== undefined) {
    if (maybe_a === "Js.Date.t") {
      return "Js.Date.t";
    }
    exit$1 = 3;
  } else {
    if (maybe_b === undefined) {
      return ;
    }
    exit$1 = 3;
  }
  if (exit$1 === 3) {
    if (maybe_b !== undefined) {
      switch (maybe_b) {
        case "Js.Date.t" :
            return "Js.Date.t";
        case "float" :
            return "float";
        default:
          exit = 2;
      }
    } else {
      exit = 2;
    }
  }
  if (exit === 2 && maybe_a !== undefined) {
    switch (maybe_a) {
      case "float" :
          return "float";
      case "int" :
          return "int";
      default:
        
    }
  }
  if (maybe_b === "int") {
    return "int";
  } else {
    return "string";
  }
}

function get_first_argument(param) {
  if (param.TAG === /* CallArguments */13) {
    return Belt_Array.getExn(param._0.positional, 0);
  }
  throw {
        RE_EXN_ID: "Match_failure",
        _1: [
          "simplifier.ml",
          163,
          25
        ],
        Error: new Error()
      };
}

function reduce_pattern_for_params(curr_type_and_acc, pattern_element) {
  var curr_acc = curr_type_and_acc[1];
  var curr_type = curr_type_and_acc[0];
  switch (pattern_element.TAG | 0) {
    case /* Pattern */3 :
        var match = Belt_Array.reduce(pattern_element._0.elements, curr_type_and_acc, reduce_pattern_for_params);
        return [
                curr_type,
                Belt_MapString.merge(match[1], curr_acc, merge_params)
              ];
    case /* Placeable */5 :
        var match$1 = reduce_pattern_for_params(curr_type_and_acc, pattern_element._0.expression);
        return [
                curr_type,
                Belt_MapString.merge(match$1[1], curr_acc, merge_params)
              ];
    case /* VariableReference */10 :
        return [
                curr_type,
                Belt_MapString.set(curr_acc, simplify_identifier(pattern_element._0.id), curr_type)
              ];
    case /* FunctionReference */11 :
        var match$2 = pattern_element._0;
        var match$3 = simplify_identifier(match$2.id);
        var param_type;
        switch (match$3) {
          case "DATETIME" :
              param_type = "Js.Date.t";
              break;
          case "NUMBER" :
              param_type = "int";
              break;
          default:
            throw {
                  RE_EXN_ID: "Match_failure",
                  _1: [
                    "simplifier.ml",
                    187,
                    23
                  ],
                  Error: new Error()
                };
        }
        var match$4 = reduce_pattern_for_params([
              param_type,
              curr_acc
            ], get_first_argument(match$2.arguments));
        return [
                curr_type,
                Belt_MapString.merge(match$4[1], curr_acc, merge_params)
              ];
    case /* SelectExpression */12 :
        var match$5 = pattern_element._0;
        var match$6 = reduce_pattern_for_params(curr_type_and_acc, match$5.selector);
        var match$7 = Belt_Array.reduce(match$5.variants, curr_type_and_acc, reduce_pattern_for_params);
        return [
                curr_type,
                Belt_MapString.merge(match$6[1], match$7[1], merge_params)
              ];
    case /* Variant */15 :
        var match$8 = reduce_pattern_for_params(curr_type_and_acc, pattern_element._0.value);
        return [
                curr_type,
                Belt_MapString.merge(match$8[1], curr_acc, merge_params)
              ];
    default:
      return [
              curr_type,
              curr_acc
            ];
  }
}

function get_pattern_params(pattern_elements) {
  return Belt_Array.reduce(pattern_elements, [
                "string",
                undefined
              ], reduce_pattern_for_params)[1];
}

function make_fn(param, $$public, namespace, lc) {
  var value = param.value;
  var name = make_fn_name(namespace, simplify_identifier(param.id), $$public);
  var pattern_elements;
  if (value == null) {
    pattern_elements = [];
  } else if (value.TAG === /* Pattern */3) {
    pattern_elements = value._0.elements;
  } else {
    throw {
          RE_EXN_ID: "Match_failure",
          _1: [
            "simplifier.ml",
            240,
            6
          ],
          Error: new Error()
        };
  }
  var params = get_pattern_params(pattern_elements);
  var simplified_pattern = simplify_pattern(pattern_elements, params);
  return {
          name: name,
          bodies: [[
              "\"" + (lc + "\""),
              simplified_pattern
            ]],
          params: params,
          public: true
        };
}

function make_entry(entry, $$public, lc) {
  var match = entry.value;
  var main_function = !(match == null) ? ({
        hd: make_fn(entry, $$public, "", lc),
        tl: /* [] */0
      }) : /* [] */0;
  var name = simplify_identifier(entry.id);
  return Belt_List.concat(main_function, Belt_List.map(Belt_List.fromArray(entry.attributes), (function (attribute) {
                    if (attribute.TAG === /* Attribute */14) {
                      return make_fn(attribute._0, $$public, name, lc);
                    }
                    throw {
                          RE_EXN_ID: "Match_failure",
                          _1: [
                            "simplifier.ml",
                            264,
                            21
                          ],
                          Error: new Error()
                        };
                  })));
}

function simplify_ast(lc, node) {
  if (node.TAG === /* Resource */0) {
    return Belt_List.toArray(Belt_List.flatten(Belt_List.fromArray(Belt_Array.map(Belt_Array.keep(node._0.body, (function (param) {
                                  switch (param.TAG | 0) {
                                    case /* Message */1 :
                                    case /* Term */2 :
                                        return true;
                                    default:
                                      return false;
                                  }
                                })), (function (entry) {
                              switch (entry.TAG | 0) {
                                case /* Message */1 :
                                    return make_entry(entry._0, true, lc);
                                case /* Term */2 :
                                    return make_entry(entry._0, false, lc);
                                default:
                                  throw {
                                        RE_EXN_ID: "Match_failure",
                                        _1: [
                                          "simplifier.ml",
                                          279,
                                          24
                                        ],
                                        Error: new Error()
                                      };
                              }
                            })))));
  }
  throw {
        RE_EXN_ID: "Match_failure",
        _1: [
          "simplifier.ml",
          269,
          2
        ],
        Error: new Error()
      };
}

exports.make_fn_name = make_fn_name;
exports.simplify_identifier = simplify_identifier;
exports.simplify_literal = simplify_literal;
exports.get_named_argument = get_named_argument;
exports.simplify_named_arguments = simplify_named_arguments;
exports.simplify_expression = simplify_expression;
exports.simplify_pattern = simplify_pattern;
exports.merge_params = merge_params;
exports.get_first_argument = get_first_argument;
exports.reduce_pattern_for_params = reduce_pattern_for_params;
exports.get_pattern_params = get_pattern_params;
exports.make_fn = make_fn;
exports.make_entry = make_entry;
exports.simplify_ast = simplify_ast;
/* No side effect */
