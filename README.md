# mel-fluent-compiler

Very dirty and hacky compiler for Fluent files to a Melange module.

## Options

- `-i glob`: glob that will match locale files, for example: `"locales/**/*.ftl"`
- `-o output`: output file, for example: `src/t.ml`
- `-d locale`: default locale, defaults to `en`
- `-l`: injects a `"!Locale.get"` if present, instead of requiring passing a locale on each function call
